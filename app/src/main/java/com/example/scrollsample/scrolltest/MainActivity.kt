package com.example.scrollsample.scrolltest

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import android.widget.ScrollView
import android.widget.TextView

/** RecyclerViewの表示項目 */
class ItemViewHolder : RecyclerView.ViewHolder {
    private var text_: TextView? = null

    public constructor(view: View) : super(view) {
        text_ = view.findViewById(R.id.text_item)
    }

    fun setup(position: Int) {
        text_?.let {it.setText(position.toString())}
    }
}

/** RecyclerViewの項目生成 */
class ItemRecyclerAdapter : RecyclerView.Adapter<ItemViewHolder> {
    private final val kItemCount = 100

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemViewHolder {
        if (parent != null) {
            val view = LayoutInflater
                    .from(parent.context)
                    .inflate(R.layout.fragment_item_view, parent, false)
            return ItemViewHolder(view)
        }
        throw NullPointerException("parent is null.")
    }

    override fun getItemCount(): Int {
        return kItemCount
    }

    override fun onBindViewHolder(holder: ItemViewHolder?, position: Int) {
        if (holder != null) {
            holder.setup(position)
        }
    }

    public constructor() {
    }
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (false) {
            // ScrollView利用
            setContentView(R.layout.activity_main)

            val header: HorizontalScrollView = findViewById(R.id.scroll_header)
            val row: ScrollView = findViewById(R.id.scroll_row)
            val itemV: ScrollView = findViewById(R.id.scroll_items_v)
            val itemH: HorizontalScrollView = findViewById(R.id.scroll_items_h)

            // ヘッダー(右上)の横すく
            header.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                itemH.scrollX = scrollX
            }
            // 項目(左下)の縦スク
            row.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                itemV.scrollY = scrollY
            }

            // アイテム(右下)の横スクと縦すく
            itemV.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                row.scrollY = scrollY
            }
            itemH.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                header.scrollX = scrollX
            }
        } else {
            // RecyclerView利用
            setContentView(R.layout.scroll_test2)

            // 右下作成
            val items: RecyclerView = findViewById(R.id.items)
            val itemLayout = LinearLayoutManager(applicationContext)
            items.setHasFixedSize(true)
            items.layoutManager = itemLayout
            items.adapter = ItemRecyclerAdapter()

            // 左下作成
            val rows: RecyclerView = findViewById(R.id.rows)
            val rowLayout = LinearLayoutManager(applicationContext)
            rows.setHasFixedSize(true)
            rows.layoutManager = rowLayout
            rows.adapter = ItemRecyclerAdapter()

            // 左下の縦スクロール
            rows.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                val offset = rows.computeVerticalScrollOffset()
                itemLayout.scrollToPositionWithOffset(0, -offset)
            }
            // 右下の縦スクロール
            items.setOnScrollChangeListener { view, scrollX, scrollY, oldScrollX, oldScrollY ->
                val offset = items.computeVerticalScrollOffset()
                rowLayout.scrollToPositionWithOffset(0, -offset)
            }
        }
    }
}
